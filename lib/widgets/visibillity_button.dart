import 'package:flutter/material.dart';
import 'package:dj_edertech/widgets/button.dart';

Visibility visibilityButtonWidget(IconData icon, bool stateCond, onPressed) {
  return Visibility(
    visible: stateCond,
    maintainState: true,
    child: buttonWidget(icon, onPressed),
  );
}
