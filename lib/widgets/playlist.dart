import 'package:dj_edertech/model/track.dart';
import 'package:flutter/material.dart';
import 'package:dj_edertech/provider/player_provider.dart';
import 'package:dj_edertech/widgets/track_tile.dart';
import 'package:provider/provider.dart';

class PlayList extends StatefulWidget {
  const PlayList({super.key});

  @override
  State<StatefulWidget> createState() => _Playlist();
}

class _Playlist extends State<PlayList> {
  @override
  Widget build(BuildContext context) {
    final playerProvider = Provider.of<PlayerProvider>(context);

    return ScrollbarTheme(
      data: ScrollbarThemeData(
        thumbColor: MaterialStateProperty.all(Colors.blue),
      ),
      child: _buildScrollbar(playerProvider),
    );
  }

  Scrollbar _buildScrollbar(PlayerProvider playerProvider) {
    return Scrollbar(
      child: ListView.builder(
        itemCount: playerProvider.tracks.length,
        itemBuilder: (BuildContext context, index) {
          final track = playerProvider.tracks[index];
          final isSelected = playerProvider.getTrackIndex() == index;
          final tileColor = isSelected ? Colors.lightBlueAccent : null;

          return _buildTrackTile(track, tileColor, playerProvider, index);
        },
      ),
    );
  }

  TrackTile _buildTrackTile(Track track, MaterialAccentColor? tileColor,
      PlayerProvider playerProvider, int index) {
    return TrackTile(
      leading: track.isNew
          ? SizedBox(
              width: 48.0,
              height: 48.0,
              child: Container(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  'assets/new_release.png',
                  width: 24.0,
                  height: 24.0,
                ),
              ),
            )
          : Container(),
      title: track.title,
      tileColor: tileColor,
      onTap: () {
        if (playerProvider.getTrackIndex() != index) {
          playerProvider.setTrackIndex(index);
          playerProvider.continuePlaying(playIfStoped: true);
        }
      },
    );
  }
}
