import 'package:flutter/material.dart';

class TrackTile extends StatelessWidget {
  final Widget leading;
  final String title;
  final String subtitle;
  final Color? tileColor;
  final GestureTapCallback? onTap;

  const TrackTile({
    super.key,
    required this.leading,
    required this.title,
    this.subtitle = '',
    required this.tileColor,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.symmetric(horizontal: 16.0),
      leading: SizedBox(
        width: 48.0,
        height: 48.0,
        child: Center(
          child: leading,
        ),
      ),
      title: Text(title),
      onTap: onTap,
      tileColor: tileColor,
      subtitle: subtitle.isNotEmpty ? Text(subtitle) : null,
    );
  }
}
