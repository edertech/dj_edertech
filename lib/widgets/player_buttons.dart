import 'package:flutter/material.dart';
import 'package:dj_edertech/provider/player_provider.dart';
import 'package:dj_edertech/widgets/button.dart';
import 'package:dj_edertech/widgets/visibillity_button.dart';
import 'package:provider/provider.dart';

class PlayerButtons extends StatefulWidget {
  const PlayerButtons({super.key});

  @override
  State<StatefulWidget> createState() => _PlayerButtons();
}

class _PlayerButtons extends State<PlayerButtons> {
  @override
  Widget build(BuildContext context) {
    final playerProvider = Provider.of<PlayerProvider>(context);
    playerProvider.start();

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        visibilityButtonWidget(Icons.play_arrow, playerProvider.isNotPlaying(),
            () => playerProvider.play()),
        visibilityButtonWidget(Icons.pause, playerProvider.isPlaying(),
            () => playerProvider.pause()),
        const SizedBox(width: 10.0),
        buttonWidget(Icons.navigate_before, () => playerProvider.previous()),
        buttonWidget(Icons.stop, () => playerProvider.stop()),
        buttonWidget(Icons.navigate_next, () => playerProvider.next()),
        const SizedBox(width: 10.0),
      ],
    );
  }
}
