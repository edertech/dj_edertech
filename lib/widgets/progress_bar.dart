import 'package:flutter/material.dart';
import 'package:dj_edertech/provider/player_provider.dart';
import 'package:provider/provider.dart';

class ProgressBar extends StatefulWidget {
  const ProgressBar({super.key});

  @override
  State<StatefulWidget> createState() => _ProgressBarState();
}

class _ProgressBarState extends State<ProgressBar> {
  @override
  Widget build(BuildContext context) {
    final playerProvider = Provider.of<PlayerProvider>(context);

    String formatDuration(Duration duration) {
      int minutes = duration.inMinutes;
      int seconds = duration.inSeconds.remainder(60);
      return '$minutes:${seconds.toString().padLeft(2, '0')}';
    }

    double sliderValue =
        playerProvider.progress.isNaN ? 0.0 : playerProvider.progress;

    return Column(
      children: [
        Slider(
          value: sliderValue.clamp(0.0, 1.0),
          onChanged: (value) => playerProvider.seekToPosition(value),
          min: 0.0,
          max: 1.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(formatDuration(playerProvider.currentPosition)),
            Text(formatDuration(playerProvider.totalDuration)),
          ],
        ),
      ],
    );
  }
}
