import 'package:audioplayers/audioplayers.dart';

class Track {
  UrlSource source;
  String title;
  bool isNew;
  String url;

  Track(this.source, this.title, this.isNew, this.url);

  Track.withURL(String url, String title, bool isNew)
      : this(UrlSource(url), title, isNew, url);
}
