import 'package:flutter/material.dart';
import 'package:dj_edertech/provider/theme_provider.dart';
import 'package:provider/provider.dart';

class ThemeSwitcher extends StatelessWidget {
  final Widget Function(BuildContext, ThemeData) builder;

  const ThemeSwitcher({super.key, required this.builder});

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);

    return builder(context, themeProvider.getTheme());
  }

  static ThemeProvider of(BuildContext context) {
    return Provider.of<ThemeProvider>(context);
  }
}
