import 'dart:collection';
import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:dj_edertech/model/track.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class PlayerProvider extends ChangeNotifier {
  int _trackIndex = 0;
  double _progress = 0;
  final AudioPlayer _player = AudioPlayer();
  PlayerState _playerState = PlayerState.stopped;
  Duration? _audioDuration;
  final List<Track> _tracks = [];
  final _catalogUrl =
      'https://gitlab.com/uploads/-/system/personal_snippet/3679137/4bd0898f2450e6069c67a5ddd9a7d8ac/all_sets_catalog.txt';

  PlayerProvider() {
    _loadCatalogDataFromRemoteServer();
    _onPlayerStateChanged();
    _onDurationChanged();
    _onPositionChanged();
  }

  void _onPlayerStateChanged() {
    _player.onPlayerStateChanged.listen((PlayerState state) {
      if (state == PlayerState.completed) {
        Future.delayed(const Duration(milliseconds: 100), () {
          stop();
        });
      }
    });
  }

  void _onDurationChanged() {
    _player.onDurationChanged.listen((Duration duration) {
      _audioDuration = duration;
      notifyListeners();

      if (_audioDuration != null) {
        notifyListeners();
      }
    });
  }

  void _onPositionChanged() {
    _player.onPositionChanged.listen((Duration duration) {
      if (_audioDuration != null && _audioDuration!.inMilliseconds > 0) {
        _progress = duration.inMilliseconds / _audioDuration!.inMilliseconds;
        notifyListeners();
      }
    });
  }

  void _setupDuration() {
    _player.onDurationChanged
        .listen((Duration duration) => _audioDuration = duration);
  }

  void start() async {
    _setupDuration();
  }

  Future<void> _loadCatalogDataFromRemoteServer() async {
    if (_tracks.isNotEmpty) return;

    final response = await http.get(Uri.parse(_catalogUrl));
    List<String> lines = const LineSplitter().convert(response.body);

    for (String line in lines) {
      List<String> values = line.split(';');
      String title = values[0];
      bool isNew = values[1] == 'true';
      String url = values[2];
      _tracks.add(Track.withURL(url, title, isNew));
    }

    notifyListeners();
  }

  UnmodifiableListView<Track> get tracks => UnmodifiableListView(_tracks);

  double get progress => _progress;

  Track getTrack() => _tracks[_trackIndex];

  int getTrackIndex() => _trackIndex;

  void setTrackIndex(int value) {
    _trackIndex = value;
    notifyListeners();
  }

  Duration get currentPosition {
    if (_audioDuration != null) {
      return Duration(
          milliseconds: (_audioDuration!.inMilliseconds * _progress).round());
    }
    return Duration.zero;
  }

  Duration get totalDuration => _audioDuration ?? Duration.zero;

  bool isPlaying() => _playerState == PlayerState.playing;

  bool isNotPlaying() => !isPlaying();

  void previous() {
    if (_trackIndex > 0) {
      _trackIndex = _trackIndex - 1;
    } else {
      _trackIndex = _tracks.length - 1;
    }
    continuePlaying();
  }

  void next() {
    if (_trackIndex < (_tracks.length - 1)) {
      _trackIndex = _trackIndex + 1;
    } else {
      _trackIndex = 0;
    }
    continuePlaying();
  }

  void continuePlaying({bool playIfStoped = false}) async {
    _progress = 0;
    if (isPlaying()) {
      await _player.stop();
      play();
    } else {
      if (playIfStoped) {
        play();
      }
    }
    notifyListeners();
  }

  void play() async {
    _playerState = PlayerState.playing;
    await _player.play(getTrack().source);
    _player.onPlayerComplete.listen((_) => stop());
    notifyListeners();
  }

  void pause() async {
    _playerState = PlayerState.paused;
    await _player.pause();
    notifyListeners();
  }

  void stop() async {
    _playerState = PlayerState.stopped;
    _progress = 0;
    _player.seek(Duration.zero);
    await _player.release();
    notifyListeners();
  }

  void seekToPosition(double value) async {
    _progress = value;
    final newPosition = (_progress * _audioDuration!.inMilliseconds).round();
    await _player.seek(Duration(milliseconds: newPosition));
    notifyListeners();
  }
}
