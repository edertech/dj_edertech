import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'license_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({super.key});

  @override
  State<AboutScreen> createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  String _version = '';
  bool _isOverViewExpanded = false;

  @override
  void initState() {
    super.initState();
    getVersion();
  }

  Future<void> getVersion() async {
    try {
      final PackageInfo packageInfo = await PackageInfo.fromPlatform();
      setState(() {
        _version = packageInfo.version;
      });
    } catch (e) {
      debugPrint('Error getting version: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildLogoApp(),
              _buildVersionHead(),
              const SizedBox(height: 8.0),
              _buildVersionBody(),
              const SizedBox(height: 24.0),
              _buildAppOverViewHead(),
              const SizedBox(height: 8.0),
              _buildAppOverViewBody(),
              _buildContact(),
              const SizedBox(height: 24.0),
              _buildLicenseHead(),
              const SizedBox(height: 8.0),
              _buildLicenseBody(),
            ],
          ),
        ),
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      title: const Text('About DJ Edertech'),
      bottom: const PreferredSize(
        preferredSize: Size.fromHeight(24.0),
        child: Padding(
          padding: EdgeInsets.only(bottom: 8.0),
          child: Text(
            'The DJ Behind the Mixes',
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  SizedBox _buildLogoApp() {
    return SizedBox(
      width: double.infinity,
      child: Column(
        children: [
          Image.asset(
            'assets/ic_launcher.png',
            width: 192.0,
            height: 192.0,
          ),
        ],
      ),
    );
  }

  Row _buildVersionHead() {
    return const Row(
      children: [
        Icon(Icons.star),
        SizedBox(width: 8.0),
        Text(
          'DJ Edertech Version',
          style: TextStyle(fontSize: 16.0),
        ),
      ],
    );
  }

  Text _buildVersionBody() {
    return Text(
      _version,
      style: const TextStyle(fontSize: 16.0),
    );
  }

  Row _buildAppOverViewHead() {
    return const Row(
      children: [
        Icon(Icons.description),
        SizedBox(width: 8.0),
        Text(
          'App Overview',
          style: TextStyle(fontSize: 16.0),
        ),
      ],
    );
  }

  InkWell _buildAppOverViewBody() {
    return InkWell(
        onTap: () {
          setState(() {
            _isOverViewExpanded = !_isOverViewExpanded;
          });
        },
        child: Column(
          children: [
            Text(
              'DJ Edertech, an enthusiast of old-school music, embarked on his DJing journey in 1989. Alongside his passion for music, he is also a skilled Software Developer and the mastermind behind this app.',
              style: const TextStyle(fontSize: 16.0),
              maxLines: _isOverViewExpanded ? null : 2,
              overflow: TextOverflow.ellipsis,
            ),
            if (_isOverViewExpanded)
              const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 16.0),
                  Text(
                    'Here, he graciously shares his curated DJ sets, inviting you to relive the golden era of music. The genres span a rich spectrum, including techno, house, breakbeat, jungle/drum & bass, synth-pop, industrial, rock, soul, disco, funk, and more.',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'Immerse yourself in the nostalgia, grooving to the beats of the past, and enjoy a seamless, immersive musical experience that brings the party to your fingertips.',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'While this app provides a glimpse into DJ Edertech\'s artistry, you can explore the entirety of his sets on his dedicated channel below. Stay tuned for future updates, including the launch of a website featuring complete sets and an enhanced app version, connecting you even further with the musical journey.',
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ),
          ],
        ));
  }

  Column _buildContact() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 24.0),
        const Row(
          children: [
            Icon(Icons.contact_support),
            SizedBox(width: 8.0),
            Text(
              'Contact',
              style: TextStyle(fontSize: 16.0),
            ),
          ],
        ),
        const SizedBox(height: 8.0),
        Row(
          children: [
            const Icon(Icons.email),
            const SizedBox(width: 8.0),
            InkWell(
              onTap: () async {
                final Uri params = Uri(
                  scheme: 'mailto',
                  path: 'djedertechshow@gmail.com',
                  query:
                      'subject=Contact%20DJ%20Edertech&body=Hello,%20DJ%20Edertech!',
                );
                var url = Uri.parse(params.toString());
                if (await canLaunchUrl(url)) {
                  await launchUrl(url);
                } else {
                  debugPrint('Could not launch $url');
                }
              },
              child: const Text(
                'Send an email',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        const SizedBox(height: 8.0),
        Row(
          children: [
            Image.asset(
              'assets/youtube.png',
              width: 24.0,
              height: 24.0,
            ),
            const SizedBox(width: 8.0),
            InkWell(
              onTap: () async {
                var url = Uri.parse('https://www.youtube.com/@djedertech');
                if (await canLaunchUrl(url)) {
                  launchUrl(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
              child: const Text(
                'Visit our YouTube channel',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Row _buildLicenseHead() {
    return const Row(
      children: [
        Icon(Icons.info),
        SizedBox(width: 8.0),
        Text(
          'Licenses',
          style: TextStyle(fontSize: 16.0),
        ),
      ],
    );
  }

  InkWell _buildLicenseBody() {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const LicenseScreen()),
        );
      },
      child: const Text(
        'Open Source License',
        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
      ),
    );
  }
}
