<img src="assets/ic_launcher.png" alt="icon">

# DJ Edertech App
This app is invites you to relive the curated DJ sets of DJ Edertech, a devoted enthusiast of old-school music. Immerse yourself in the nostalgic beats spanning various music genres, including techno, house, breakbeat, jungle/drum & bass, synth-pop, industrial, rock, soul, disco, funk, and more.


## App Overview
DJ Edertech, a DJ since 1989 and a skilled Software Developer, is the mastermind behind this app. The app offers a glimpse into his artistry, sharing curated sets that invite you to relive the golden era of music.

- Explore a rich spectrum of genres.
- Enjoy a seamless, immersive musical experience.

## App Themes
This will be updated as the program evolves.

| <img src="images/phoneScreenshots/print-1.jpg" width="200"/> | <img src="images/phoneScreenshots/print-2.jpg" width="200"/> |
|:---:|:---:|
|Dark| Clearly white|

## Download
[<img src="/uploads/1ee5cd9326dcae6880a4a6727bac36ac/logo.png"
    alt="Get it on GitLab"
    height="80">](https://gitlab.com/edertech/dj_edertech/-/releases/1.0.0)

[<img src="https://f-droid.org/badge/get-it-on.png"
    alt="Get it on GitLab"
    height="80">](https://f-droid.org/en/packages/com.edertech.dj_edertech)


## Setup and install
First, clone the repository with the 'clone' command, or just download the zip.

```
$ git clone git@gitlab.com:edertech/dj_edertech.git
```

Then, download your preferred editor (Android Studio, Intellij or Visual Studio Code), with their respective [Flutter editor plugins](https://flutter.io/get-started/editor/). For more information about Flutter installation procedure, go to the the [official install guide](https://flutter.io/get-started/install/).

Install dependencies from pubspec.yaml by running `flutter packages get` from the project root (see [using packages documentation](https://flutter.io/using-packages/#adding-a-package-dependency-to-an-app) for details and how to do this in your preferred editor).

There you go, you can now open & edit the project.

## Built with

- [Flutter](https://flutter.dev/) - Beautiful native apps in record time.
- [IntelliJ IDEA Community Edition](https://github.com/JetBrains/intellij-community) Code editing. Redefined.
- [Android Studio](https://developer.android.com/studio/index.html/) - Tools for building apps on every type of Android device.
## Contact
- **Email:** [djedertechshow@gmail.com](mailto:djedertechshow@gmail.com)
- **YouTube Channel:** [Visit our YouTube channel](https://www.youtube.com/@djedertech)

## Licenses
- [Open Source License](/LICENSE)
